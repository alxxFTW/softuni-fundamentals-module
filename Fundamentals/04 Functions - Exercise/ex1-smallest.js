function smallestV1(...arr) {

  console.log(Math.min.apply(Number, arr));

}

function smallestV2(...arr) {

  console.log(Math.min(...arr));

}

smallestV1(2, 5, 3);
smallestV1(600, 342, 123);
smallestV1(25, 21, 4);

smallestV2(2, 5, 3);
smallestV2(600, 342, 123);
smallestV2(25, 21, 4);