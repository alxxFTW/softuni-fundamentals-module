function factorialize(num) {

  if (num < 0) {

    return -1;

  } else if (num == 0) {

    return 1;

  } else {

    return (num * factorialize(num - 1));

  }

}

function solve(num1, num2) {

  console.log((factorialize(num1) / factorialize(num2)).toFixed(2));

}

solve(6, 2);
solve(5, 2);
