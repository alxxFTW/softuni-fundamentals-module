function sum(num1, num2) {

  return num1 + num2;

}

function subtract(result, num3) {

  return result - num3;

}

function solve(num1, num2, num3) {

  let result;

  result = sum(num1, num2);
  result = subtract(result, num3);

  console.log(result);

}

solve(23, 6, 10);
solve(1, 17, 30);
solve(42, 58, 100);