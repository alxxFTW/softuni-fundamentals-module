function getIndex(char) {

  return char.charCodeAt(0);

}

function cycle(index1, index2) {

  let arr = [];

  for (let i = index1 + 1; i < index2; i++) {
    
    arr.push(String.fromCharCode(i));

  }

  return arr;

}

function printChars(char1, char2) {

  let arr = [];
  let indexChar1 = getIndex(char1);
  let indexChar2 = getIndex(char2);

  if (char1 < char2) {
    
    arr = cycle(indexChar1, indexChar2);

  } else {
    
    arr = cycle(indexChar2, indexChar1);

  }

  console.log(...arr);

}

printChars('#', ':');
printChars('a', 'd');
printChars('C', '#');