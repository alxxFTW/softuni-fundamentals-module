function sum(arr) {
  let sumEven = 0;
  let sumOdd = 0;
  let sums = [];

  arr.forEach((element) => {

    if (element % 2 === 0) {

      sumEven += Number(element);

    } else {

      sumOdd += Number(element);

    }

  });

  sums.push(sumEven);
  sums.push(sumOdd);

  return sums;

}

function sums(str) {

  let arr = str.toString().split('');

  arr = sum(arr);

  console.log(`Odd sum = ${arr[1]}, Even sum = ${arr[0]}`);

}

sums(1000435);
sums(3495892137259234);