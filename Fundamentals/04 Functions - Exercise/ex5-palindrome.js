function palindromeCheck(number) {

  let reversedNumber = number.toString().split('').reverse().join('');

  return (reversedNumber == number) ? true : false;

}

function palindrome(arr) {

  arr.forEach((number) => {

    if (palindromeCheck(number)) {

      console.log('true');

    } else {

      console.log('false');

    }

  });

}

palindrome([123, 323, 421, 121]);
palindrome([32, 2, 232, 1010]);