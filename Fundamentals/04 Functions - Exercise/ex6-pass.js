function lengthCheck(pass) {

  return pass.length >= 6 && pass.length <= 10 ? true : false;

}

function symbolCheck(pass) {

  let regex = new RegExp(/[\d, A-z]/, 'g');

  return pass.replace(regex, '').length === 0 ? true : false;

}

function digitCheck(pass) {

  let regex = new RegExp(/[^\d]/, 'g');

  return pass.replace(regex, '').length >= 2 ? true : false;

}

function checkPass(pass) {

  let validLength = lengthCheck(pass);
  let noSymbols = symbolCheck(pass);
  let validDigitCount = digitCheck(pass);

  if (validLength === false) console.log("Password must be between 6 and 10 characters");
  if (noSymbols === false) console.log("Password must consist only of letters and digits");
  if (validDigitCount === false) console.log("Password must have at least 2 digits");

  if (validLength && noSymbols && validDigitCount) console.log("Password is valid");

}

checkPass('1Pa$s$s123');
console.log('================');
checkPass('logIn');
console.log('================');
checkPass('MyPass123');
console.log('================');
checkPass('Pa$s$s');

