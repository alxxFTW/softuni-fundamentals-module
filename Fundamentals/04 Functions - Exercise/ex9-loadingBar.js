function finished(num) {

  return num === 100 ? true : false;

}

function createBar(num) {

  let arr = new Array(10).fill('.').fill('%', 0, num / 10);

  return `[${arr.join('')}]`;

}

function solve(num) {

  if (finished(num)) {

    console.log('100% Complete!');
    console.log(`${createBar(num)}`);

  } else {

    console.log(`${num}% ${createBar(num)}`);
    console.log('Still loading...');

  }

}

solve(30);
solve(50);
solve(100);