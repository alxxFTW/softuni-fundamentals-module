function graded(grade) {

  if (grade < 3) {
    return 'Fail';
  }

  else if (grade < 3.50) {
    return 'Poor';
  }

  else if (grade < 4.50) {
    return 'Good';
  }

  else if (grade < 5.50) {
    return 'Very Good';
  }

  else if (grade <= 6.00) {
    return 'Excelent';
  }

}

console.log(graded(3.33));
console.log(graded(4.50));
console.log(graded(2.99));
console.log(graded(5.49));
console.log(graded(6.00));