function price(product, quantity) {

  prices = {
    'coffee': 1.50,
    'water': 1.00,
    'coke': 1.40,
    'snacks': 2.00
  };

  return (prices[product] * quantity).toFixed(2);

}

console.log(price('water', 5));
console.log(price('coffee', 2));