function calc(numOne, numTwo, operator) {

  let result;

  if (operator === 'multiply') {

    result = numOne * numTwo;

  } else if (operator === 'divide') {

    result = numOne / numTwo;

  } else if (operator === 'add') {

    result = numOne + numTwo;

  } else if (operator === 'subtract') {

    result = numOne - numTwo;

  }

  return result;

}

console.log(calc(5, 5, 'multiply'));
console.log(calc(40, 8, 'divide'));
console.log(calc(12, 19, 'add'));
console.log(calc(50, 13, 'subtract'));