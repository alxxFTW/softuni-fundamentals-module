function wrgRes(numOne, numTwo, numThree) {

  let result = numOne * numTwo * numThree;

  return result < 0 ? 'Negative' : 'Positive';

}

console.log(wrgRes(5, 12, -15));
console.log(wrgRes(-6, -12, 14));
console.log(wrgRes(-1, -2, -3));
console.log(wrgRes(-1, 0, 1));