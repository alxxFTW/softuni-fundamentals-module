function train(arr) {

  let wagons = arr.shift().split(' ').map(Number);

  let maxCapacity = Number(arr.shift());

  arr.forEach((cmd) => {

    cmd = cmd.split(' ');

    number = Number(cmd[cmd.length - 1]);

    command = cmd[0];

    if (cmd.includes('Add')) {

      wagons.push(Number(number));

    } else {

      for (let index = 0; index <= wagons.length - 1; index++) {

        let space = maxCapacity - wagons[index];

        if (space >= number) {

          wagons[index] = number + wagons[index];

          break;

        }

      }

    }

  });

  console.log(...wagons);

}

train(['32 54 21 12 4 0 23', '75', 'Add 10', 'Add 0', '30', '10', '75']);

console.log('===========');

train(['0 0 0 10 2 4', '10', 'Add 10', '10', '10', '10', '8', '6']);