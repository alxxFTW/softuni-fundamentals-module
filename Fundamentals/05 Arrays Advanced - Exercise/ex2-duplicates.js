function duplicates(numbers) {

  for (let i = 0; i < numbers.length; i++) {

    for (let k = i + 1; k < numbers.length; k++) {

      if (numbers[i] === numbers[k]) {

        numbers.splice(k, 1);

      }

    }

  }

  console.log(...numbers);

}

duplicates([1, 2, 3, 4]);
duplicates([7, 8, 9, 7, 2, 3, 4, 1, 2]);
duplicates([20, 8, 12, 13, 4, 4, 8, 5]);
