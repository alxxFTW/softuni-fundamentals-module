function party(arr) {

  let guestList = [];

  arr.forEach((guestState) => {

    let guestName = guestState.split(' ')[0];

    if (guestState.includes('is going')) {

      if (guestList.includes(guestName)) {

        console.log(`${guestName} is already in the list!`);

      } else {

        guestList.push(guestName);

      }

    } else {

      if (guestList.includes(guestName)) {

        guestList.splice(guestList.indexOf(guestName), 1);

      } else {

        console.log(`${guestName} is not in the list!`);

      }

    }

  });

  guestList.forEach(guest => {

    console.log(guest);

  });

}

party(
  [
    'Allie is going!',
    'George is going!',
    'John is not going!',
    'George is not going!'
  ]
);

party(
  [
    'Tom is going!',
    'Annie is going!',
    'Tom is going!',
    'Garry is going!',
    'Jerry is going!'
  ]
);