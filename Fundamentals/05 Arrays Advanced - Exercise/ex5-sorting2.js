function sorting2(arr) {

  arr.sort((a, b) => {
    return a.length - b.length || a.localeCompare(b);
  });

  arr.forEach(element => {
    console.log(element);
  });

}

sorting2(["alpha", "beta", "gamma"]);
sorting2(["Isacc", "Theodor", "Jack", "Harrison", "George"]);