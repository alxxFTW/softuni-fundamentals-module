function detonation(numbers, data) {

  let detonator = data[0];
  let power = data[1];

  let result = 0;

  numbers.map((number, index) => {

    if (number === detonator) {

      numbers.splice(index, power + 1);
      numbers.splice(index - power, power);

      console.log(numbers);

    }

  });
  result = numbers.reduce((a, b) => { return a + b; });
  console.log(result);

}

detonation([1, 2, 2, 4, 2, 2, 2, 9], [4, 5]);
detonation([1, 4, 4, 2, 8, 9, 1], [9, 3]);
detonation([1, 7, 7, 1, 2, 3], [7, 1]);
detonation([1, 1, 2, 1, 1, 1, 2, 1, 1, 1], [2, 1]);