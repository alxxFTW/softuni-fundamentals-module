function duplicates(numbers) {

  let count = [];
  let res = [];

  numbers.forEach(number => {
    if (!count[number]) {
      res.push(number);
    }

    count[number] = true;
  });

  console.log(res);

}

duplicates([1, 2, 3, 4]);
duplicates([7, 8, 9, 7, 2, 3, 4, 1, 2]);
duplicates([20, 8, 12, 13, 4, 4, 8, 5]);
