function sumFirstAndLast(arr) {

  arr.splice(1, arr.length - 2);

  let sum = arr.map(Number).reduce(function (first, last) {
    return first + last;
  });

  console.log(sum);

}

sumFirstAndLast(['20', '30', '40']);
sumFirstAndLast(['5', '10']);