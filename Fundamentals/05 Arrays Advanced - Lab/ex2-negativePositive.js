function negPos(arr) {

  let newArr = [];

  arr.forEach((element) => {

    if (element >= 0) newArr.push(element);
    else newArr.unshift(element);

  });

  newArr.forEach((element) => {

    console.log(element);

  });

}

negPos([7, -2, 8, 9]);
negPos([3, -2, 0, -1]);