function lastSeq(n, k) {

  let newArr = [1];

  for (let i = 1; i <= n - 1; i++) {

    let currentSumArr = newArr.slice(-k);
    
    let sum = 0;

    for (let number of currentSumArr) {

      sum += number;

    }

    newArr.push(sum);

  }

  console.log(...newArr);

}



lastSeq(6, 3);
lastSeq(8, 2);