function prcsOdds(arr) {

  let prcsArr = [];

  arr.forEach((element, index) => {

    if (index % 2 !== 0) {

      prcsArr.push(element * 2);

    }

  });

  prcsArr = prcsArr.reverse();

  console.log(...prcsArr);

}

prcsOdds([10, 15, 20, 25]);
prcsOdds([3, 0, 10, 4, 7, 3]);