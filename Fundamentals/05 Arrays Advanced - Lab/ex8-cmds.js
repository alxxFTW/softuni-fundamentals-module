function arrMnplt(arr) {

  let numbers = arr.shift(arr).split(' ');

  arr.map((cmd) => {

    if (cmd.includes('Add')) {

      numbers.push(cmd.split(' ')[1]);
     
    } else if (cmd.includes('RemoveAt')) {

      numbers.splice(cmd.split(' ')[1], 1);
     
    } else if (cmd.includes('Remove')) {

      numbers = numbers.filter(element => {
        
        return element != cmd.split(' ')[1];

      });
    
    } else if (cmd.includes('Insert')) {

      numbers.splice(cmd.split(' ')[2], 0, cmd.split(' ')[1]);
      
    }

  });

  console.log(...numbers);

}

arrMnplt(['4 19 2 53 6 43', 'Add 3', 'Remove 2', 'RemoveAt 1', 'Insert 8 3']);