function emps(arr) {

  class Employee {

    constructor(name) {
  
      this.name = name;
      this.number = name.length;
      this.creds = () => {
        console.log(`Name: ${name} -- Personal Number: ${this.number}`);
      };
  
    }
  
  }

  arr.forEach(name => {

    let employee = new Employee(name);

    employee.creds();

  });

}

emps([
  'Silas Butler',
  'Adnaan Buckley',
  'Juan Peterson',
  'Brendan Villarreal'
]);