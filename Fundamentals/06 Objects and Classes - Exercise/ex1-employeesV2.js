function emps(arr) {

  arr.forEach(name => {

    let person = {

      name,
      number: name.length

    };

    console.log(`Name: ${person.name} -- Personal Number: ${person.number}`);

  });

}

emps([
  'Silas Butler',
  'Adnaan Buckley',
  'Juan Peterson',
  'Brendan Villarreal'
]);