function townsCords(cities) {

  class Town {

    constructor(city, latitude, longitude) {

      this.city = city;
      this.latitude = latitude;
      this.longitude = longitude;
      this.giveInfo = () => {
        console.log(`{ town: '${city.trim()}', latitude: '${Number(latitude).toFixed(2)}', longitude: '${Number(longitude).toFixed(2)}' }`);
      };

    }

  }

  cities.forEach(city => {

    let [town, latitude, longitude] = city.split('|');

    let townCords = new Town(town, latitude, longitude);

    townCords.giveInfo();

  });

}

townsCords(
  ['Sofia | 42.696552 | 23.32601',
    'Beijing | 39.913818 | 116.363625']
);