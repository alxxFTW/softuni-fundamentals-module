function store(storage, delivery) {

  function restock(arr, obj) {

    for (let i = 0; i < arr.length; i += 2) {

      const product = arr[i];
      const quantity = Number(arr[i + 1]);

      if (!obj.hasOwnProperty(product)) {
        obj[product] = 0;
      }

      obj[product] += quantity;

    }

    return obj;

  }

  let storeInventory = restock(storage, {});
  let productsAfterDelivery = restock(delivery, storeInventory);

  for (let [product, quantity] of Object.entries(productsAfterDelivery)) {

    console.log(`${product} -> ${quantity}`);

  }

}

store(
  [
    'Chips', '5', 'CocaCola', '9', 'Bananas', '14', 'Pasta', '4', 'Beer', '2'
  ],
  [
    'Flour', '44', 'Oil', '12', 'Pasta', '7', 'Tomatoes', '70', 'Bananas', '30'
  ]
);
