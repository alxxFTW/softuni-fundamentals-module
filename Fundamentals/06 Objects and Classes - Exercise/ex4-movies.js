function movies(cmdList) {

  function movieObj(movies, movie) {

    return movies.find((movies) => movies.name === movie);

  }

  let movies = [];

  cmdList.forEach(cmd => {

    let movie, director, date;

    if (cmd.includes('addMovie')) {

      movie = cmd.split(' ').slice(1).join(' ');
      movies.push({ name: movie });

    } else if (cmd.includes('directedBy')) {

      [movie, director] = cmd.split(' directedBy ');

      let film = movieObj(movies, movie);

      if (film) {
        film.director = director;
      }

    } else {

      [movie, date] = cmd.split(' onDate ');

      let film = movieObj(movies, movie);

      if (film) {
        film.date = date;
      }

    }

  });

  movies.forEach(movie => {

    if (Object.keys(movie).length === 3) console.log(JSON.stringify(movie));

  });

}


movies(
  [
    'addMovie Fast and Furious',
    'addMovie Godfather',
    'Inception directedBy Christopher Nolan',
    'Godfather directedBy Francis Ford Coppola',
    'Godfather onDate 29.07.2018',
    'Fast and Furious onDate 30.07.2018',
    'Batman onDate 01.08.2018',
    'Fast and Furious directedBy Rob Cohen'
  ]
);