class Vehicle {

  constructor(type, model, parts, fuel) {

    this.type = type;
    this.model = model;
    this.parts = {...parts, quality: parts.engine * parts.power};
    this.fuel = fuel;

  }

  drive(fuelLoss) {
    this.fuel -= fuelLoss;
  }

}

let vehicleParts = {
  engine: 100,
  power: 2,
  quality: 160
};
let myVehicle = new Vehicle('Opel', 'Astra', vehicleParts, 190);

console.log(myVehicle);

myVehicle.drive(50);

console.log(myVehicle);