function city(name, area, population, country, postCode) {

  let city = {
    name,
    area,
    population,
    country,
    postCode
  };

  let props = Object.keys(city);

  props.forEach(prop => {

    console.log(`${prop} -> ${city[prop]}`);

  });

}

city("Sofia", " 492", "1238438", "Bulgaria", "1000");