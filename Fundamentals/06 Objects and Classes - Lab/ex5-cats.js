function cat(cats) {

  class Cat {

    constructor(name, age) {

      this.name = name;
      this.age = age;
      this.meow = () => {
        console.log(`${name}, age ${age} says Meow`);
      };

    }

  }

  cats.forEach(cat => {

    let [name, age] = cat.split(' ');

    cat = new Cat(name, age);

    cat.meow();

  });

}

cat(['Mellow 2', 'Tom 5']);