function songs(arr) {

  class PlayList {

    constructor(toPlay, songPlaylist, song) {
      
      this.toPlay = toPlay;
      this.songPlaylist = songPlaylist;
      this.song = song;
      this.play = () => {
    
        if (songPlaylist === toPlay) {
          console.log(song);
        } else if (toPlay === "all") {
          console.log(song);
        }
  
      };
  
    }
  
  }

  arr.shift();

  let toPlay = arr.pop();

  arr.forEach(row => {

    let [songPlaylist, song] = row.split('_');

    let currently = new PlayList(toPlay, songPlaylist, song);

    currently.play();

  });

}

songs([3,
  'favourite_DownTown_3:14',
  'favourite_Kiss_4:16',
  'favourite_Smooth Criminal_4:01',
  'favourite']
);

songs([4,
  'favourite_DownTown_3:14',
  'listenLater_Andalouse_3:24',
  'favourite_In To The Night_3:58',
  'favourite_Live It Up_3:48',
  'listenLater']
);

songs([2,
  'like_Replay_3:15',
  'ban_Photoshop_3:48',
  'all']
);
