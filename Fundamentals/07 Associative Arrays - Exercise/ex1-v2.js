function wordsTracker(input) {

  let tracker = {};

  let [soughtWords, ...words] = input;

  soughtWords.split(' ').forEach((word) => {
    tracker[word] = 0;
  });

  words.forEach(word => {
    if (tracker.hasOwnProperty(word)) {
      tracker[word] += 1;
    }
  });

  Object.keys(tracker)
    .sort((a, b) => tracker[b] - tracker[a])
    .forEach((el) => {
      console.log(`${el} - ${tracker[el]}`);
    });

}

wordsTracker([
  'this sentence',
  'In', 'this', 'sentence', 'you', 'have',
  'to', 'count', 'the', 'occurances', 'of', 'the',
  'words', 'this', 'and', 'sentence', 'because',
  'this', 'is', 'your', 'task'
]);