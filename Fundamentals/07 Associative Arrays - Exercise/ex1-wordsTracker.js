function wordsTracker(input) {

  let [soughtWords, ...words] = input;

  let checkFor = new Map();

  soughtWords.split(' ').forEach((word) => {

    checkFor.set(word, 0);

  });

  words.forEach(word => {

    if (checkFor.has(word)) {

      checkFor.set(word, checkFor.get(word) + 1);

    }

  });

  let sortedlist = Array.from(checkFor.entries()).sort((a, b) => b[1] - a[1]);

  sortedlist.forEach(element => {

    let [word, count] = element;
    console.log(`${word} - ${count}`);

  });

}

wordsTracker([
  'this sentence',
  'In', 'this', 'sentence', 'you', 'have',
  'to', 'count', 'the', 'occurances', 'of', 'the',
  'words', 'this', 'and', 'sentence', 'because',
  'this', 'is', 'your', 'task'
]);