function oddOccurances(input) {

  let tracker = {};

  input
    .split(' ')
    .map(word => word.toLowerCase())
    .forEach(word => {

      if (tracker[word]) {
        tracker[word] += 1;
      } else {
        tracker[word] = 1;
      }

    });
  console.log(tracker);
  
  Object.keys(tracker).filter(key => tracker[key] % 2 === 1).forEach(key => { console.log(key);});

}

oddOccurances('Java C# Php PHP Java PhP 3 C# 3 1 5 C#');