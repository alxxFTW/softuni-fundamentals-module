function oddOccurances(input) {

  let tracker = new Map();

  input
    .split(' ')
    .forEach(word => {

      word = word.toLowerCase();

      if (tracker.has(word)) {

        tracker.set(word, tracker.get(word) + 1);

      } else {

        tracker.set(word, 1);

      }
    });
  
  console.log(tracker);

  console.log(
    Array
      .from(tracker.keys())
      .filter(key => tracker.get(key) % 2 === 1)
      .join(' ')
  );

}

oddOccurances('Java C# Php PHP Java PhP 3 C# 3 1 5 C#');