function parking(input) {

  let parkingLot = [];

  input.forEach(line => {

    let [direction, number] = line.split(', ');

    if (direction === 'IN' && !parkingLot.includes(number)) {

      parkingLot.push(number);

    } else if (direction === 'OUT') {

      parkingLot = parkingLot.filter(sn => sn !== number);

    }

  });

  if (parkingLot.length === 0) {

    console.log('Parking Lot is Empty');

  } else {

    parkingLot.sort((a, b) => a.localeCompare(b)).forEach(number => console.log(number));

  }

}

parking([
  'IN, CA2844AA',
  'IN, CA1234TA',
  'OUT, CA2844AA',
  'IN, CA9999TT',
  'IN, CA2866HI',
  'OUT, CA1234TA',
  'IN, CA2844AA',
  'OUT, CA2866HI',
  'IN, CA9876HH',
  'IN, CA2822UU'
]);

parking(['IN, CA2844AA',
  'IN, CA1234TA',
  'OUT, CA2844AA',
  'OUT, CA1234TA']
);