function party(input) {

  let attendants = input
    .splice(input.indexOf('PARTY'))
    .slice(1);

  let guestList = input;

  let list = {
    vip: [],
    regular: [],
    total: 0
  };

  guestList.forEach(attendee => {

    if (Number.isInteger(+attendee[0])) {
      list.vip.push(attendee);
    } else {
      list.regular.push(attendee);
    }

    list.total++;

  });

  attendants.forEach(attendee => {

    if (list.vip.includes(attendee)) {

      list.vip.splice(list.vip.indexOf(attendee), 1);

    } else if (list.regular.includes(attendee)) {

      list.regular.splice(list.regular.indexOf(attendee), 1);

    }

    list.total--;

  });

  console.log(list.total);
  list.vip.forEach(attendee => console.log(attendee));
  list.regular.forEach(attendee => console.log(attendee));

}

party([
  '7IK9Yo0h',
  '9NoBUajQ',
  'Ce8vwPmE',
  'SVQXQCbc',
  'tSzE5t0p',
  'PARTY',
  '9NoBUajQ',
  'Ce8vwPmE',
  'SVQXQCbc'
]);

// party([
//   'm8rfQBvl',
//   'fc1oZCE0',
//   'UgffRkOn',
//   '7ugX7bm0',
//   '9CQBGUeJ',
//   '2FQZT3uC',
//   'dziNz78I',
//   'mdSGyQCJ',
//   'LjcVpmDL',
//   'fPXNHpm1',
//   'HTTbwRmM',
//   'B5yTkMQi',
//   '8N0FThqG',
//   'xys2FYzn',
//   'MDzcM9ZK',
//   'PARTY',
//   '2FQZT3uC',
//   'dziNz78I',
//   'mdSGyQCJ',
//   'LjcVpmDL',
//   'fPXNHpm1',
//   'HTTbwRmM',
//   'B5yTkMQi',
//   '8N0FThqG',
//   'm8rfQBvl',
//   'fc1oZCE0',
//   'UgffRkOn',
//   '7ugX7bm0',
//   '9CQBGUeJ'
// ]);

