function travelTime(input) {

  let list = {};

  input.forEach(line => {

    let [country, city, cost] = line.split(' > ');

    if (!list.hasOwnProperty(country)) {
      list[country] = [];
    }

    let currentTown = list[country].find((o) => o.city === city);

    if (!currentTown) {
      list[country].push({ city, cost });
    } else if ((+cost) < currentTown.cost) {
      currentTown.cost = (+cost);
    }

  });

  Object.keys(list)
    .sort((a, b) => a.localeCompare(b))
    .forEach(country => {
      let output = `${country} -> `;
      list[country]
        .sort((a, b) => a.cost - b.cost)
        .forEach(o => {
          output += `${o.city} -> ${o.cost} `;
        });

      console.log(output);

    });

}

travelTime([
  "Bulgaria > Sofia > 500",
  "Bulgaria > Sopot > 800",
  "France > Paris > 2000",
  "Albania > Tirana > 1000",
  "Bulgaria > Sofia > 200"
]);