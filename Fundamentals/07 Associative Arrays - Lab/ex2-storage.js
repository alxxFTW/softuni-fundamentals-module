function solve(input) {

  let storage = new Map();

  input.forEach(element => {

    let [product, quantity] = element.split(' ');

    quantity = Number(quantity);

    if (storage.has(product)) {

      storage.set(product, storage.get(product) + quantity);

    } else {

      storage.set(product, quantity);

    }

  });

  for (const [product, quantity] of storage) {
    console.log(`${product} -> ${quantity}`);
  }

}

solve(['tomatoes 10', 'coffee 5', 'olives 100', 'coffee 40']);