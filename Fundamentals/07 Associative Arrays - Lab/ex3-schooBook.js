
function schoolBook(input) {

  function average(arr) {
    return arr.reduce((a, b) => { return a + b / arr.length; });
  }

  let jurnal = new Map();

  input.forEach(element => {

    let [student, ...grades] = element.split(' ');
    grades = grades.map(Number);

    if (jurnal.has(student)) {

      jurnal.set(student, jurnal.get(student).concat(grades));

    } else {

      jurnal.set(student, grades);

    }

  });

  let sorted = Array.from(jurnal.entries()).sort((a, b) =>

    average(a[1]) - average(b[1])

  );

  sorted.forEach(element => {
    console.log(`${element[0]}: ${element[1].join(', ')}`);
  });

}

schoolBook(['Lilly 4 6 6 5', 'Tim 5 6', 'Tammy 2 4 3', 'Tim 6 6']);