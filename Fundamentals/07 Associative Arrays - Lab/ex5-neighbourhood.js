function solve(input) {
  
  let nbhds = new Map();

  let [nbhdsList, ...cmds] = input;

  nbhdsList.split(', ').forEach(nbh => {

    nbhds.set(nbh, []);

  });

  cmds.forEach(cmd => {

    let [nbhdName, resident] = cmd.split(' - ');

    if (nbhds.has(nbhdName)) {
      nbhds.get(nbhdName).push(resident);
    }

  });

  let sortedByPopulation = Array.from(nbhds.entries())
    .sort((a, b) => b[1].length - a[1].length);

  sortedByPopulation.forEach(entry => {

    let [nbhd, residents] = entry;

    console.log(`${nbhd}: ${residents.length}`);

    residents.forEach(resident => {

      console.log(`--${resident}`);

    });

  });

}

solve([
  'Abbey Street, Herald Street, Bright Mews',
  'Bright Mews - Garry',
  'Bright Mews - Andrea',
  'Invalid Street - Tommy',
  'Abbey Street - Billy'
]);