// let assocArr = {};

// assocArr['one'] = 1;
// assocArr['two'] = 1;
// assocArr['three'] = 1;
// assocArr['two'] = 5;

// Object.keys(assocArr).forEach((key) => {
//   console.log(`${key} => ${assocArr[key]}`);
// });


// function namesAndNumbers(titties) {

//   let tittieBook = {};

//   titties.forEach(tittie => {

//     let [name, number] = tittie.split(' ');

//     tittieBook[name] = number;

//   });

//   Object.keys(tittieBook).forEach((quickfuck) => {

//     console.log(`${quickfuck} -> ${tittieBook[quickfuck]}`);

//   });

// }

// namesAndNumbers([
//   'Tim 0834212554',
//   'Peter 0877547887',
//   'Bill 0896543112',
//   'Tim 0876566344'
// ]);

function solve(input) {

  let storage = new Map();

  input.forEach(element => {

    let [product, quantity] = element.split(' ');

    quantity = Number(quantity);

    if (storage.has(product)) {

      storage.set(product, storage.get(product) + quantity);

    } else {

      storage.set(product, quantity);

    }

  });

  for (const [product, quantity] of storage) {
    console.log(`${product} -> ${quantity}`);
  }

}

solve(['tomatoes 10', 'coffee 5', 'olives 100', 'coffee 40']);