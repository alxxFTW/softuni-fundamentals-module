function furnitures(input) {

  let totalSum = 0;
  
  input.forEach(line => {

    const pattern = />>(?<name>.+)<<(?<price>[\d]+\.?[\d]+)!(?<quantity>[\d]+)/g;

    const matches = pattern.exec(line);

    if (matches) {

      let { name, price, quantity } = matches.groups;

      totalSum += (+price) * (+quantity);

    }

  });

}

furnitures([
  '>>Sofa<<312.23!3',
  '>>TV<<300!5',
  '>Invalid<<!5',
  'Purchase'
]);