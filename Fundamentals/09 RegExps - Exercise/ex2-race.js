function race(input) {

  const participants = input.slice(0, 1)[0].split(', ');
  const raceInfo = input.slice(1, input.length - 1);
  const racers = {};

  raceInfo.forEach(line => {

    const namePattern = /[A-Za-z]/g;
    const digitPattern = /[\d]/g;

    const matchedName = line.match(namePattern);
    const matchedDistance = line.match(digitPattern);

    if (matchedName && matchedDistance) {

      const name = matchedName.join('');

      if (participants.includes(name)) {

        const distance = matchedDistance.map(Number).reduce((a, b) => a + b, 0);

        if (!racers[name]) {

          racers[name] = 0;

        }

        racers[name] += distance;

      }

    }

    console.log(racers);

  });


}

race([
  'George, Peter, Bill, Tom',
  'G4e@55or%6g6!68e!!@',
  'R1@!3a$y4456@',
  'B5@i@#123ll',
  'G@e54o$r6ge#',
  '7P%et^#e5346r',
  'T$o553m&6',
  'end of race'
]);