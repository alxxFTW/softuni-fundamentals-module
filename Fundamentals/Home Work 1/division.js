function division(number) {

    let maxDivisor = Number.NEGATIVE_INFINITY;
    let divisors = [10, 7, 6, 3, 2];

    for (let divisor of divisors) {

        if (number % divisor == 0) {

            maxDivisor = divisor;
            break;

        }

    }

    (maxDivisor === Number.NEGATIVE_INFINITY) ?
        console.log('Not divisible') : console.log(`The number is divisible by ${maxDivisor}`);

}

division(30);
division(15);
division(12);
division(1643);