function numberRotation(numbersStart, numbersEnd) {

    let sequence = [];
    let rotatingNumber;

    /*
	Add all the numbers in a single sequence
    NOTE: Can be removed in case you always start from 1
	*/
    for (numbersStart; numbersStart <= numbersEnd; numbersStart++) {
        sequence.push(numbersStart);
    }

    /*
    Simple for loop to go throught the elements of the array
    Started from 0 so I can print the 1st row... 
    There is probably a more efficient way to do it
    */
    for (let i = 0; i < sequence.length - 1; i++) {

        //Printing out the first row of the sequence
        if (i === 0) console.log(...sequence);

        //Storing the last number of the array to be used
        rotatingNumber = sequence[sequence.length - 1] -1;

        //Remove the first element of the array
        sequence.shift();

        /*
        Since it's a simple and monotonous decrease we can simply
        add the rotating element
        */
        sequence.push(rotatingNumber);

        console.log(...sequence);

    }
}

numberRotation(2, 7);