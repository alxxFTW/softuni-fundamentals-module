function login(arr){

    let username = arr.shift();
    let fails = 0;

    for (let i = 0; i < arr.length; i++){

        let password = arr[i].split('').reverse().join('');

        if (username === password){
            console.log(`User ${username} logged in.`);
        }else if (fails === 3){
            console.log(`User ${username} blocked!`);
            break;
        }else {
            console.log('Incorrect password. Try again.');
            fails++;
        }
    }

}
login(['Acer','login','go','let me in','recA']);
login(['momo','omom']);
login(['sunny','rainy','cloudy','sunny','not sunny']);
