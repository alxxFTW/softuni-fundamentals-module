function mining(wages) {

    let pricePerBitcoin = 11949.16;
    let priceOfGold = 67.51;
    let miningDays = 0;
    let dailyLeva = 0;
    let totalLeva = 0;
    let totalBitcoin = 0;
    let purchasedBitcoin = false;
    let purchases = [];

    for (let i = 0; i < wages.length; i++) {
        miningDays++;
        dailyLeva = 0;

        if (miningDays % 3 === 0) dailyLeva = wages[i] * 0.70 * priceOfGold;
        else dailyLeva = wages[i] * priceOfGold;

        totalLeva += dailyLeva;

        while (totalLeva >= pricePerBitcoin) {
            
            purchasedBitcoin = true;
            purchases.push(i + 1);

            totalBitcoin++;
            totalLeva -= pricePerBitcoin;

        }

    }
    console.log(`Bought bitcoins: ${totalBitcoin}`);
    if (purchasedBitcoin) console.log(`Day of the first purchased bitcoin: ${purchases[0]}`);
    console.log(`Left money: ${totalLeva.toFixed(2)} lv.`);

}

console.log("=====================");
console.log("=====================");
console.log("=====================");
mining([100, 200, 300]);
console.log("=====================");
console.log("=====================");
console.log("=====================");
mining([50, 100]);
console.log("=====================");
console.log("=====================");
console.log("=====================");
mining([3124.15, 504.212, 2511.124]);
console.log("=====================");
console.log("=====================");
console.log("=====================");