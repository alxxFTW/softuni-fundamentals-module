function pyramidOfJoser(baseWidth, increment) {

    let totalStone = 0;
    let totalMarble = 0;
    let totalLapis = 0;
    let totalGold = 0;
    let floors = 0;
    let counter = 0;
    let currentSideSize = 0;
    let currentBulk = 0;
    let currentOuterLayer = 0;
    let currentStepArea = 0;


    for (let currentStepSize = baseWidth; currentStepSize > 0; currentStepSize -= 2) {

        currentSideSize = currentStepSize;
        counter++;

        if ((currentStepSize === 1) || (currentStepSize === 2 && counter % 5 !== 0)) {

            totalGold = Math.pow(currentSideSize, 2) * increment;

        } else if (counter % 5 === 0) {

            currentStepArea = Math.pow(currentStepSize, 2) * increment;
            currentBulk = (Math.pow(currentStepSize - 2, 2)) * increment;
            currentOuterLayer = Math.ceil(currentStepArea - currentBulk);
            totalLapis += currentOuterLayer;
            totalStone += currentBulk;
            currentOuterLayer = 0;
            currentBulk = 0;

        }else {

            currentStepArea = Math.pow(currentStepSize, 2) * increment;
            currentBulk = (Math.pow(currentStepSize - 2, 2)) * increment;
            currentOuterLayer = Math.ceil(currentStepArea - currentBulk);
            totalMarble += currentOuterLayer;
            totalStone += currentBulk;
            currentOuterLayer = 0;
            currentBulk = 0;

        }

    }

    floors = Math.floor(counter * increment);

    console.log(`Stone required: ${Math.ceil(totalStone)}`);
    console.log(`Marble required: ${Math.ceil(totalMarble)}`);
    console.log(`Lapis Lazuli required: ${Math.ceil(totalLapis)}`);
    console.log(`Gold required: ${Math.ceil(totalGold)}`);
    console.log(`Final pyramid height: ${floors}`);

}
console.log("===========================");
console.log("===========================");
console.log("===========================");
pyramidOfJoser(11, 1);
console.log("===========================");
console.log("===========================");
console.log("===========================");
pyramidOfJoser(11, 0.75);
console.log("===========================");
console.log("===========================");
console.log("===========================");
pyramidOfJoser(12, 1);
console.log("===========================");
console.log("===========================");
console.log("===========================");
pyramidOfJoser(23, 0.5);
console.log("===========================");
console.log("===========================");
console.log("===========================");