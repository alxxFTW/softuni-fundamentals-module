function solve(count, group, day) {
    let price, totalPrice, discount;

    price = {
        "Students": {
            "Friday": 8.45,
            "Saturday": 9.80,
            "Sunday": 10.46
        },
        "Business": {
            "Friday": 10.90,
            "Saturday": 15.60,
            "Sunday": 16
        },
        "Regular": {
            "Friday": 15,
            "Saturday": 20,
            "Sunday": 22.50
        }
    };

    totalPrice = price[group][day] * count;

    if (count >= 30 && group === "Students") {
        discount = totalPrice * 0.15;
        totalPrice -= discount;
    } else if (count >= 100 && group === "Business") {
        totalPrice = price[group][day] * (count - 10);
    } else if (count >= 10 && count <= 20 && group === "Regular") {
        discount = totalPrice * 0.05;
        totalPrice -= discount;
    }

    console.log("Total price: " + totalPrice.toFixed(2));
}

solve(30,
    "Students",
    "Sunday"
);
