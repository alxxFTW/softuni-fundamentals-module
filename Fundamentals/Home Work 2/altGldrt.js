function gldtr(lostCount, priceHlm, priceSwrd, priceShld, priceArm) {

    let counter = 0;
    let total = 0;

    total += Math.floor(lostCount / 2) * priceHlm;
    total += Math.floor(lostCount / 3) * priceSwrd;

    for (let currentLose = 1; currentLose <= lostCount; currentLose++) {

        if (currentLose % 3 === 0 && currentLose % 2 === 0) {
            total += priceShld;
            counter++;
        }
        if (counter === 2) {
            total += priceArm;
            counter = 0;
        }

    }

    console.log(`Gladiator expenses: ${(total).toFixed(2)} aureus`);
}

gldtr(7, 2, 3, 4, 5);
gldtr(23, 12.50, 21.50, 40, 200);