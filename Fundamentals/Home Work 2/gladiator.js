function gldtr(lostCount, priceHlm, priceSwrd, priceShld, priceArm) {

    let totalHlm = 0;
    let totalSwrd = 0;
    let totalShld = 0;
    let totalArm = 0;
    let counter = 0;

    totalHlm = Math.floor(lostCount / 2) * priceHlm;
    totalSwrd = Math.floor(lostCount / 3) * priceSwrd;

    for (let currentLose = 1; currentLose <= lostCount; currentLose++) {

        if (currentLose % 3 === 0 && currentLose % 2 === 0) {
            totalShld += priceShld;
            counter++;
        }
        if (counter === 2) {
            totalArm += priceArm;
            counter = 0;
        }

    }

    console.log(`Gladiator expenses: ${(totalHlm + totalSwrd + totalShld + totalArm).toFixed(2)} aureus`);
}

gldtr(7, 2, 3, 4, 5);
gldtr(23, 12.50, 21.50, 40, 200);