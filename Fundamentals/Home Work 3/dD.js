function dd(rooms) {

  let currentHealth = 100, coins = 0, counter = 0, isWinner = true, arr = rooms.toString().split('|');

  for (let room of arr) {

    counter++;

    if (room.includes('potion')) {

      let currentRoomHealth = currentHealth + Number(room.split(' ')[1]);

      if (currentRoomHealth > 100) {

        console.log(`You healed for ${100 - currentHealth} hp.`);
        currentHealth = 100;

      } else if (currentRoomHealth <= 100) {

        currentHealth += Number(room.split(' ')[1]);
        console.log(`You healed for ${room.split(' ')[1]} hp.`);

      }

      console.log(`Current health: ${currentHealth} hp.`);

    } else if (room.includes('chest')) {

      coins += Number(room.split(' ')[1]);
      console.log(`You found ${room.split(' ')[1]} coins.`);

    } else {

      currentHealth -= room.split(' ')[1];

      if (currentHealth <= 0) {

        console.log(`You died! Killed by ${room.split(' ')[0]}.`);
        console.log(`Best room: ${counter}`);
        isWinner = false;

        break;

      } else {

        console.log(`You slayed ${room.split(' ')[0]}.`);

      }

    }

  }

  if (isWinner === true) {

    console.log(`You've made it!`);
    console.log(`Coins: ${coins}`);
    console.log(`Health: ${currentHealth}`);

  }

}

// dd('rat 10|bat 20|potion 10|rat 10|chest 100|boss 70|chest 1000');
// You slayed rat.
// You slayed bat.
// You healed for 10 hp.
// Current health: 80 hp.
// You slayed rat.
// You found 100 coins.
// You died! Killed by boss.
// Best room: 6

dd('cat 10|potion 30|orc 10|chest 10|snake 25|chest 110');
// You slayed cat.
// You healed for 10 hp.
// Current health: 100 hp.
// You slayed orc.
// You found 10 coins.
// You slayed snake.
// You found 110 coins.
// You've made it!
// Coins: 120
// Health: 65
