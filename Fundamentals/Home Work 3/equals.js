function sum(arr, beginning, end) {

  return arr.slice(beginning, end).reduce((a, b) => a + b, 0);

}

function equals(arr) {

  let sumRight, sumLeft, noEquality = true;

  for (let i = 0; i < arr.length; i++) {

    sumRight = sum(arr, i + 1, arr.length);
    sumLeft = sum(arr, 0, i);

    if (sumLeft === sumRight) {

      console.log(i);

      noEquality = false;

      return;

    }

  }

  if (noEquality) {

    console.log('no');

  }

}

equals([1, 2, 3, 3]);
equals([1, 2]);
equals([1]);
equals([1, 2, 3]);
equals([10, 5, 5, 99, 3, 4, 2, 5, 1, 1, 4]);