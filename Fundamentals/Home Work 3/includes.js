function filterR(arrA, arrB) {

  let common = arrA.filter(x => arrB.includes(x));

  common.forEach(elem => console.log(String(elem)));

}

filterR(
  ['Hey', 'hello', 2, 4, 'Peter', 'e'],
  ['Petar', 10, 'hey', 4, 'hello', '2']
);

filterR(
  ['S', 'o', 'f', 't', 'U', 'n', 'i', ' '],
  ['s', 'o', 'c', 'i', 'a', 'l']
);