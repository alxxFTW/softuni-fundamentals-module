function longestSqnc(arr) {

  let currentSqnc = [], longestSqnc = [];

  arr.forEach((number, index) => {

    if (index === 0) return;

    if (number !== arr[index - 1]) {

      if (currentSqnc.length > longestSqnc.length) {

        longestSqnc = currentSqnc;

      }

      currentSqnc = [number];

    } else {

      currentSqnc.push(number);

    }

  });

  if (currentSqnc.length > longestSqnc.length) {

    longestSqnc = currentSqnc;

  }

  console.log(...longestSqnc);

}


longestSqnc([2, 1, 1, 2, 3, 3, 2, 2, 2, 1]);
longestSqnc([1, 1, 1, 2, 3, 1, 3, 3]);
longestSqnc([4, 4, 4, 4]);
longestSqnc([0, 1, 1, 5, 2, 2, 6, 3, 3]);