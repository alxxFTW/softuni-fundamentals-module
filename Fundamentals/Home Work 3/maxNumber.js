function maxNumber(arr) {

  let higherNumbs = [];

  for (let i = 0; i < arr.length; i++) {

    let isHigher = true;

    for (let k = i + 1; k < arr.length; k++) {
      if (arr[i] < arr[k]) {
        isHigher = false;
        break;
      }
    }

    if (isHigher) {
      higherNumbs.push(arr[i]);
    }

  }

  console.log(...higherNumbs);

}

maxNumber([1, 4, 3, 2]);
maxNumber([14, 24, 3, 19, 15, 17]);
maxNumber([41, 41, 34, 20]);
maxNumber([27, 19, 42, 2, 13, 45, 48]);