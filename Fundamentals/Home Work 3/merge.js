function whatever(arrA, arrB) {

  let newArr = [];

  for (let i = 0; i < arrA.length; i++) {

    if (i % 2 === 0) {
      newArr.push(Number(arrA[i]) + Number(arrB[i]));
    } else {
      newArr.push(String(arrA[i]) + String(arrB[i]));
    }

  }

  console.log(newArr.join(' - '));

}

whatever(
  ['5', '15', '23', '56', '35'],
  ['17', '22', '87', '36', '11']
);

whatever(
  ['13', '12312', '5', '77', '4'],
  ['22', '333', '5', '122', '44']
);