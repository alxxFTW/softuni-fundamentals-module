function charity(input) {

  let [string, ...commands] = input;
  commands = commands.slice(0, commands.length - 1);

  commands.forEach(line => {

    let [command, ...rest] = line.split(' ');

    if (command === 'Replace') {

      let [char, replacement] = rest;

      let pattern = new RegExp(char, 'g');

      string = string.replace(pattern, replacement);

      console.log(string);

    } else if (command === 'Make') {

      let type = rest.join('');

      string = type === 'Lower' ? string.toLowerCase() : string.toUpperCase();

      console.log(string);

    } else if (command === 'Check') {

      let doesItContainThis = rest.join('');

      console.log(
        string.includes(doesItContainThis) ?
          `Message contains ${doesItContainThis}` :
          `Message doesn't contain ${doesItContainThis}`
      );

    } else if (command === 'Sum') {

      let [startIndex, endIndex] = rest;

      if (startIndex < 0 || endIndex < 0 || startIndex > string.length - 1 || startIndex > endIndex || endIndex > string.length - 1) {

        return console.log("Invalid indexes!");

      } else {

        let substring = string.substring(startIndex, ++endIndex);

        let sum = 0;

        substring
          .split('')
          .forEach(str => {

            sum += str.charCodeAt(0);

          });

        console.log(sum);

      }

    } else if (command === 'Cut') {

      let [startIndex, endIndex] = rest;

      if (startIndex < 0 || endIndex < 0 || startIndex > string.length - 1 || startIndex > endIndex || endIndex > string.length - 1) {

        return console.log("Invalid indexes!");

      } else {

        let removeThis = string.substring(startIndex, ++endIndex);

        if (string.includes(removeThis)) {

          string = string.replace(removeThis, '');

        }

        console.log(string);

      }

    }

  });

}

charity([
  'ILikeSharan',
  'Replace a e',
  'Make Upper',
  'Check SHEREN',
  'Sum 1 4',
  'Cut 1 4',
  'Finish'
]);

charity([
  'HappyNameDay',
  'Replace p r',
  'Make Lower',
  'Cut 2 23',
  'Sum -2 2',
  'Finish'
]);

charity([
  'DinnerIsServed',
  'Make Upper',
  'Check Dinner',
  'Replace N M',
  'Finish'
]);