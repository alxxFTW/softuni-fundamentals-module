function meals(input) {

  let commands = input.slice(0, input.length - 1);
  
  let guestList = {};
  let dislikes = 0;

  commands.forEach(line => {

    let [command, name, dish] = line.split('-');

    if (command === 'Like') {

      if (!guestList[name]) {

        guestList[name] = [];

      }

      if (!guestList[name].includes(dish)) {

        guestList[name].push(dish);

      }

    } else {

      if (!guestList[name]) {

        console.log(`${name} is not at the party.`);

      } else {

        if (!guestList[name].includes(dish)) {

          console.log(`${name} doesn't have the ${dish} in his/her collection.`);

        } else {

          let indexOfDisliked = guestList[name].indexOf(dish);

          guestList[name].splice(indexOfDisliked, 1);
          dislikes += 1;

          console.log(`${name} doesn't like the ${dish}.`);

        }

      }

    }

  });

  let guests = Object.keys(guestList);

  if (guests.length > 0) {

    guests
      .sort((guestOne, guestTwo) => guestList[guestTwo].length - guestList[guestOne].length || guestOne.localeCompare(guestTwo))
      .forEach(guest => console.log(`${guest}: ${guestList[guest].join(', ')}`));
  }

  console.log(`Unliked meals: ${dislikes}`);

}

meals([
  'Like-Krisi-shrimps',
  'Like-Krisi-soup',
  'Like-Misho-salad',
  'Like-Pena-dessert',
  'Stop'
]);

meals([
  'Like-Krisi-shrimps',
  'Unlike-Vili-carp',
  'Unlike-Krisi-salad',
  'Unlike-Krisi-shrimps',
  'Stop'
]); 

meals([
  'Like-Mike-frenchFries',
  'Like-Mike-salad',
  'Like-George-fruit',
  'Like-Steven-salad',
  'Unlike-Steven-salad',
  'Unlike-Steven-fruit',
  'Stop'
]);