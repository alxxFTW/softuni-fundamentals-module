function messageTranslator(input) {

  input.shift();

  input.forEach(line => {

    let pattern = /!(?<doThis>[A-Z][a-z]{2,})!:\[(?<message>.{8,})\]/g;

    let match = pattern.exec(line);

    if (match) {

      const { doThis, message } = match.groups;

      let messageNumbers = [];

      message
        .split('')
        .forEach(letter => messageNumbers.push(letter.charCodeAt(0)));

      console.log(`${doThis}: ${messageNumbers.join(' ')}`);

    } else {

      console.log("The message is invalid");

    }

  });

}

messageTranslator([
  '2',
  '!Send!:[IvanisHere]',
  '*Time@:[Itis5amAlready]'
]);

messageTranslator([
  '3',
  'go:[outside]',
  '!drive!:YourCarToACarWash',
  '!Watch!:[LordofTheRings]'
]);

messageTranslator([
  '3',
  '!play!:[TheNewSong]',
  '!Ride!:[Bike]',
  '!Watch!:[LordofTheRings]'
]);