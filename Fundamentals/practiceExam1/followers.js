function followers(input) {

  function newFollower(data, username) {

    if (!data.hasOwnProperty(username)) {

      data[username] = {

        likes: 0,
        comments: 0

      };

    }

    return data;

  }

  let data = {};

  input.forEach(line => {

    let [cmd, ...parameters] = line.split(': ');

    if (cmd === 'New follower') {

      let username = parameters;

      data = newFollower(data, username);

    } else if (cmd === 'Like') {

      let [username, likes] = parameters;

      data = newFollower(data, username);
      data[username].likes += Number(likes);

    } else if (cmd === 'Comment') {

      let [username] = parameters;

      data = newFollower(data, username);
      data[username].comments += 1;

    } else if (cmd === 'Blocked') {

      let username = parameters;

      if (data.hasOwnProperty(username)) {

        delete data[username];

      } else {

        console.log(`${username} doesn't exist.`);

      }

    }

  });

  const followers = Object.keys(data);

  console.log(`${followers.length} followers`);

  if (followers.length > 0) {

    followers
      .sort((followerOne, followerTwo) => data[followerTwo].likes - data[followerOne].likes || followerOne.localeCompare(followerTwo))
      .forEach(follower => {

        const total = data[follower].likes + data[follower].comments;

        console.log(`${follower}: ${total}`);

      });

  }

}

followers([
  'New follower: gosho',
  'Like: gosho: 5',
  'Comment: gosho',
  'New follower: gosho',
  'New follower: tosho',
  'Comment: gosho',
  'Comment: tosho',
  'Comment: pesho',
  'Log out'
]);

followers([
  'Like: A: 3',
  'Comment: A',
  'New follower: B',
  'Blocked: A',
  'Comment: B',
  'Like: C: 5',
  'Like: D: 5',
  'Log out'
]);