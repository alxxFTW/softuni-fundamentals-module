function password(input) {

  let [needless, ...passwords] = input;

  passwords.forEach(password => {

    let pattern = /(?<start>.+)>(?<numbers>[\d]{3})\|(?<lowerCaseLetters>[a-z]{3})\|(?<upperCaseLetters>[A-Z]{3})\|(?<allChars>[^<>]{3})<(\k<start>)/g;

    let match = pattern.exec(password);
    console.log(match);

    if (match) {

      const { numbers, lowerCaseLetters, upperCaseLetters, allChars } = match.groups;
      console.log(`Password: ${numbers + lowerCaseLetters + upperCaseLetters + allChars}`);

    } else {

      console.log("Try another password!");

    }

  });

}

password([
  '3',
  '##>00|no|NO|!!!?<###',
  '##>123|yes|YES|!!!<##',
  '$$<111|noo|NOPE|<<>$$'
]);

// password([
//   '5',
//   'aa>111|mqu|BAU|mqu<aa',
//   '()>111!aaa!AAA!^&*<()',
//   'o>088|abc|AAA|***<o',
//   'asd>asd|asd|ASD|asd<asd',
//   '*>088|zzzz|ZzZ|123<*'
// ]);