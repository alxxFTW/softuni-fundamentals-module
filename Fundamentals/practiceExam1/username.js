function username(input) {

  let [username, ...commands] = input;

  commands.forEach(command => {

    let [cmd, paramOne, paramTwo] = command.split(' ');

    if (cmd === 'Case') {

      username = paramOne === 'lower' ? username.toLowerCase() : username.toUpperCase();

      console.log(username);

    } else if (cmd === 'Reverse') {

      let startIndex = Number(paramOne);
      let endIndex = Number(paramTwo);

      if (startIndex < 0 || startIndex > username.length - 1 || startIndex > endIndex || endIndex > username.length - 1) {

        return;

      }

      const substring = username.substring(startIndex, endIndex + 1).split('').reverse().join('');

      console.log(substring);

    } else if (cmd === 'Cut') {

      let substring = paramOne;

      if (!username.includes(substring)) {

        console.log(`The word ${username} doesn't contain ${substring}`);

      } else {

        const index = username.indexOf(substring);

        username = username.substring(0, index) + username.substring(index + substring.length);
        console.log(username);

      }

    } else if (cmd === 'Replace') {

      let charToReplace = paramOne;
      let pattern = new RegExp(charToReplace, 'g');

      username = username.replace(pattern, '*');

      console.log(username);

    } else if (cmd === 'Check') {

      let char = paramOne;

      if (!username.includes(char)) {

        console.log(`Your username must contain ${char}.`);

      } else {

        console.log('Valid');

      }

    }

  });

}

username([
  'Pesho',
  'Case lower',
  'Cut ES',
  'Check @',
  'Sign up',
]);

username([
  'ThisIsMyString',
  'Reverse 1 4',
  'Replace i',
  'Cut My',
  'Sign up'
]);