function solve(input) {

  let string = input.shift();
  let commands = input.slice(0, input.length - 1);

  commands.forEach(command => {

    let [doThis, ...followUp] = command.split(' ');

    doThis = doThis.trim();

    if (doThis.includes('Translate')) {

      let [replaceThis, replacewith] = followUp;
      let regexp = new RegExp(replaceThis, 'g');

      string = string.replace(regexp, replacewith);
      console.log(string);

    } else if (doThis.includes('Includes')) {

      let doesItInclude = followUp.join(' ').trim();

      console.log(string.includes(doesItInclude) ? 'True' : 'False');

    } else if (doThis.includes('Start')) {

      let doesItStartWith = followUp.join(' ').trim();

      console.log(string.startsWith(doesItStartWith) ? 'True' : 'False');

    } else if (doThis.includes('Lowercase')) {

      string = string.toLowerCase();

      console.log(string);

    } else if (doThis.includes('FindIndex')) {

      let indexТоFind = followUp.join(' ').trim();

      console.log(string.lastIndexOf(indexТоFind));

    } else if (doThis.includes('Remove')) {

      let [startIndex, charCount] = followUp;

      let end = Number(startIndex) + Number(charCount);

      string = string.substring(0, startIndex) + string.substring(end);

      console.log(string);

    }

  });

}

solve([
  '//Thi5 I5 MY 5trING!//',
  'Translate 5 s',
  'Includes string',
  'Start //This',
  'Lowercase',
  'FindIndex i',
  'Remove 0 10',
  'End'
]);