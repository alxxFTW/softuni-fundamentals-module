function solve(input) {

  let rgx = /^([$%])([A-Z][a-z]{2,})\1:\s\[(\d+)\]\|\[(\d+)\]\|\[(\d+)\]\|$/;

  input.forEach(command => {

    let matches = command.match(rgx);

    if (matches) {

      console.log(`${matches[2]}: ${String.fromCharCode(matches[3],matches[4],matches[5])}`);

    } else {

      console.log('Invalid');

    }

  });

}

solve([
  '$Request$: [73]|[115]|[105]|',
  '%Taggy$: [73]|[73]|[73]',
  '%Taggy%: [118]|[97]|[108]|',
  '$Request$: [73]|[115]|[105]|[32]|[72]|'
]);