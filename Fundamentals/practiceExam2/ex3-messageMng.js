function solve(input) {

  let limit = Number(input.shift());
  let data = {};

  input.forEach(row => {

    let [cmd, ...rest] = row.split('=');

    if (cmd === 'Add') {

      let [user, ...msgsRest] = rest;

      if (!data.hasOwnProperty(user)) {

        let [sentMsgsCount, recievedMsgsCount] = msgsRest;

        data[user] = {
          sent: Number(sentMsgsCount),
          received: Number(recievedMsgsCount)
        };

      }

    } else if (cmd === 'Message') {

      let [user1, user2] = rest;

      if (data[user1] && data[user2]) {

        data[user1].sent++;
        data[user2].received++;

        if (data[user1].sent + data[user1].received === limit) {

          delete data[user1];
          console.log(`${user1} reached the capacity!`);
  
        } else if (data[user2].sent + data[user2].received === limit) {
  
          delete data[user2];
          console.log(`${user2} reached the capacity!`);
  
        }

      }

    } else if (cmd === 'Empty') {

      if (rest.join(' ').trim() == 'All') {

        data = {};

      } else if (data[rest.join(' ').trim()]) {

        delete data[rest];

      }

    } else if (cmd === 'Statistics') {

      let comperator = (a, b) =>
        b[1].received - a[1].received || a[0].localeCompare(b[0]);

      let arr = Object.entries(data).sort(comperator);

      console.log(`Users count: ${arr.length}`);

      arr.forEach(userStats => {

        let userName = userStats.shift();
        let sentMsges = userStats[0].sent;
        let receivedMsges = userStats[0].received;

        console.log(`${userName} - ${sentMsges + receivedMsges}`);

      });

    }

  });

}

solve([
  '10',
  'Add=Mark=5=4',
  'Add=Clark=3=5',
  'Add=Berg=9=0',
  'Add=Kevin=0=0',
  'Message=Berg=Kevin',
  'Statistics'
]);